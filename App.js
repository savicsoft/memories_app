import React from "react";
import { StyleSheet, View } from "react-native";
import englishUnitedStates from "./src/i18n/en-us";
import serbianLatin from "./src/i18n/sr-Latn";
import swedishSweden from "./src/i18n/sv-se";
import * as Localization from "expo-localization";
import i18n from "i18n-js";
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import Main from "./src/components/main/Main";

i18n.translations = {
  en: englishUnitedStates,
  sr: serbianLatin,
  sv: swedishSweden,
};

i18n.locale = Localization.locale;
i18n.fallbacks = true;

export default function App() {
  return (
    <NavigationContainer>
      <View style={styles.container}>
        <Main />
      </View>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
