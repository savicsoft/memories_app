import Query from "../model/Query";

export const createTableQueries = [
  new Query(
    "CREATE TABLE IF NOT EXISTS memory ( id	TEXT NOT NULL PRIMARY KEY, user_id	TEXT NOT NULL, title	TEXT NOT NULL, description	TEXT NOT NULL, image	BLOB, mood_indicator	TEXT, date TEXT)",
    []
  ),
  new Query(
    "CREATE TABLE IF NOT EXISTS user (id	TEXT NOT NULL PRIMARY KEY, user_name	TEXT NOT NULL, first_name	TEXT, last_name	TEXT, age	TEXT, email	TEXT, gender	TEXT, password TEXT)",
    []
  ),
];

export const dropTableQueries = [
  new Query("DROP TABLE IF EXISTS memory", []),
  new Query("DROP TABLE IF EXISTS user", []),
];
