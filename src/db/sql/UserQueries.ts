export const insertUserQuery =
  "INSERT INTO user (user_name, first_name, last_name, age, email, gender, id, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

export const getUserQuery = "SELECT * FROM user WHERE user.id = ?";

export const getUserByUsernameQuery =
  "SELECT * FROM user WHERE user.user_name = ?";

export const getLatestUserQuery = "SELECT * FROM user";

export const updateUserQuery =
  "UPDATE user SET user_name = ?, first_name = ?, last_name = ?, age = ?, email = ?, gender = ?, password = ? WHERE user.id = ?";

export const deleteUserQuery = "DELETE from user WHERE user.id = ?";
