export const insertMemoryQuery =
  "INSERT INTO memory (user_id, title, description, image, mood_indicator, date, id) VALUES (?, ?, ?, ?, ?, ?, ?)";

export const getMemoryQuery = "SELECT * FROM memory WHERE memory.id = ?";

export const updateMemoryQuery =
  "UPDATE memory SET user_id = ?, title = ?, description = ?, image = ?, mood_indicator = ?, date = ? WHERE memory.id = ?";

export const deleteMemoryQuery = "DELETE from memory WHERE memory.id = ?";

export const getAllMemoriesQuery = "SELECT * FROM memory WHERE user_id = ?";
