import { getUserByUsernameQuery } from "./../sql/UserQueries";
import {
  insertUserQuery,
  getUserQuery,
  updateUserQuery,
  deleteUserQuery,
  getLatestUserQuery,
} from "../sql/UserQueries";
import { executeQuery } from "../DatabaseConnection";
import User from "../model/User";
import Query from "../model/Query";

export const insertUser = (user: User) => {
  return executeQuery(
    new Query(insertUserQuery, [
      user.userName,
      user.firstName,
      user.lastName,
      user.age,
      user.email,
      user.gender,
      user.id,
      user.password,
    ])
  );
};

export const findUser = (id: string) => {
  return executeQuery(new Query(getUserQuery, [id]));
};

export const findUserByUsername = (userName: string) => {
  return executeQuery(new Query(getUserByUsernameQuery, [userName]));
};

export const findLatestUser = () => {
  return executeQuery(new Query(getLatestUserQuery, []));
};

export const updateUser = (user: User) => {
  return executeQuery(
    new Query(updateUserQuery, [
      user.userName,
      user.firstName,
      user.lastName,
      user.age,
      user.email,
      user.gender,
      user.id,
      user.password,
    ])
  );
};

export const removeUser = (id: string) => {
  return executeQuery(new Query(deleteUserQuery, [id]));
};
