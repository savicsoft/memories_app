import {
  insertMemoryQuery,
  getMemoryQuery,
  updateMemoryQuery,
  deleteMemoryQuery,
  getAllMemoriesQuery,
} from "../sql/MemoryQueries";
import { executeQuery } from "../DatabaseConnection";
import Memory from "../model/Memory";
import Query from "../model/Query";

export const insertMemory = (memory: Memory) => {
  return executeQuery(
    new Query(insertMemoryQuery, [
      memory.userId,
      memory.title,
      memory.description,
      memory.image,
      memory.moodIndicator,
      memory.date,
      memory.id,
    ])
  );
};

export const findMemory = (id: string) => {
  return executeQuery(new Query(getMemoryQuery, [id]));
};

export const findAllMemories = (userId: string) => {
  return executeQuery(new Query(getAllMemoriesQuery, [userId]));
};

export const updateMemory = (memory: Memory) => {
  return executeQuery(
    new Query(updateMemoryQuery, [
      memory.userId,
      memory.title,
      memory.description,
      memory.image,
      memory.moodIndicator,
      memory.date,
      memory.id,
    ])
  );
};

export const removeMemory = (id: string) => {
  return executeQuery(new Query(deleteMemoryQuery, [id]));
};
