import * as SQLite from "expo-sqlite";
import constants from "../constants/Constants";
import Query from "./model/Query";
import DbInit from "./model/DbInit";

export const initDb = (dbInit: DbInit) => {
  return SQLite.openDatabase(
    dbInit.name,
    dbInit.version,
    dbInit.description,
    dbInit.size
  );
};

export const executeQuery = async (query: Query) => {
  const db = initDb(new DbInit(constants.databaseName));

  return new Promise((resolve) =>
    db.transaction(
      (tx) => {
        tx.executeSql(query.sql, query.args, (_, { rows }) => resolve(rows)),
          (error) => {
            console.log("Unable to execute query: " + error.message);
          };
      },
      (error) => {
        console.log("Unable to get transaction: " + error.message);
      }
    )
  );
};
