export default class Query {
  sql: string;
  args: string[];

  constructor(sql: string, args: string[]) {
    this.sql = sql;
    this.args = args;
  }
}
