import User from "../User";
import uuid from "react-native-uuid";

export default class UserBuilder {
  private _id: string = "";
  private _userName: string = "";
  private _firstName: string = "";
  private _lastName: string = "";
  private _age: string = "";
  private _email: string = "";
  private _gender: string = "";
  private _password: string = "";

  setId = (id: string) => {
    this._id = id;
    return this;
  };

  setUserName = (userName: string) => {
    this._userName = userName;
    return this;
  };

  setFirstName = (firstName: string) => {
    this._firstName = firstName;
    return this;
  };

  setLastName = (lastName: string) => {
    this._lastName = lastName;
    return this;
  };

  setAge = (age: string) => {
    this._age = age;
    return this;
  };

  setEmail = (email: string) => {
    this._email = email;
    return this;
  };

  setGender = (gender: string) => {
    this._gender = gender;
    return this;
  };

  setPassword = (password: string) => {
    this._password = password;
    return this;
  };

  build = () => {
    return new User(this);
  };

  get id() {
    return this._id;
  }

  get userName() {
    return this._userName;
  }

  get firstName() {
    return this._firstName;
  }

  get lastName() {
    return this._lastName;
  }

  get age() {
    return this._age;
  }

  get email() {
    return this._email;
  }

  get gender() {
    return this._gender;
  }

  get password() {
    return this._password;
  }

  generateId = () => {
    this._id = uuid.v4();
    return this;
  };
}
