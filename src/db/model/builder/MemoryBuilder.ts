import Memory from "../Memory";
import uuid from "react-native-uuid";

export default class MemoryBuilder {
  private _id: string;
  private _userId: string;
  private _title: string;
  private _description: string;
  private _image: string;
  private _moodIndicator: string;
  private _date: string;

  setId = (id: string) => {
    this._id = id;
    return this;
  };

  setUserId = (userId: string) => {
    this._userId = userId;
    return this;
  };

  setTitle = (title: string) => {
    this._title = title;
    return this;
  };

  setDescription = (description: string) => {
    this._description = description;
    return this;
  };

  setImage = (image: string) => {
    this._image = image;
    return this;
  };

  setMoodIndicator = (moodIndicator: string) => {
    this._moodIndicator = moodIndicator;
    return this;
  };

  setDate = (date: string) => {
    this._date = date;
    return this;
  };

  build = () => {
    return new Memory(this);
  };

  get id() {
    return this._id;
  }

  get userId() {
    return this._userId;
  }

  get title() {
    return this._title;
  }

  get description() {
    return this._description;
  }

  get image() {
    return this._image;
  }

  get moodIndicator() {
    return this._moodIndicator;
  }

  get date() {
    return this._date;
  }

  generateId = () => {
    this._id = uuid.v4();
    return this;
  };
}
