import UserBuilder from "./builder/UserBuilder";
export default class User {
  id: string;
  userName: string;
  firstName: string;
  lastName: string;
  age: string;
  email: string;
  gender: string;
  password: string;

  constructor(userBuilder: UserBuilder) {
    this.id = userBuilder.id;
    this.userName = userBuilder.userName;
    this.firstName = userBuilder.firstName;
    this.lastName = userBuilder.lastName;
    this.age = userBuilder.age;
    this.email = userBuilder.email;
    this.gender = userBuilder.gender;
    this.password = userBuilder.password;
  }
}
