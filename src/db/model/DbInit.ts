export default class DbInit {
  name: string;
  description: string;
  version: string;
  size: number;

  constructor(name, description = "", version = "", size = 0) {
    this.name = name;
    this.description = description;
    this.version = version;
    this.size = size;
  }
}
