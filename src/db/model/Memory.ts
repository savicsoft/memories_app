import MemoryBuilder from "./builder/MemoryBuilder";

export default class Memory {
  id: string;
  userId: string;
  title: string;
  description: string;
  image: string;
  moodIndicator: string;
  date: string;

  constructor(memoryBuilder: MemoryBuilder) {
    this.id = memoryBuilder.id;
    this.userId = memoryBuilder.userId;
    this.title = memoryBuilder.title;
    this.description = memoryBuilder.description;
    this.image = memoryBuilder.image;
    this.moodIndicator = memoryBuilder.moodIndicator;
    this.date = memoryBuilder.date;
  }
}
