import { executeQuery } from "./DatabaseConnection";
import { createTableQueries, dropTableQueries } from "./sql/TableQueries";

export const initializeTables = async () => {
  return Promise.all(createTableQueries.map(executeQuery));
};

export const dropTables = async () => {
  return Promise.all(dropTableQueries.map(executeQuery));
};
