import { Alert } from "react-native";

export const simpleAlert = (alertTitle, alertMsg) => {
  Alert.alert(
    alertTitle,
    alertMsg,
    [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    { cancelable: false }
  );
};
