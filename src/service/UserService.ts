import {
  findUser,
  insertUser,
  updateUser,
  removeUser,
  findUserByUsername,
  findLatestUser,
} from "./../db/repo/UserRepo";
import User from "../db/model/User";
import UserBuilder from "../db/model/builder/UserBuilder";

export const addUser = async (user: User) => {
  return Promise.resolve(
    insertUser(user).then((res) => {
      return res;
    })
  );
};

export const getUser = async (id: string) => {
  return Promise.resolve(
    findUser(id).then((rows) => {
      return mapUser(rows);
    })
  );
};

export const getUserByUsername = async (userName: string) => {
  return Promise.resolve(
    findUserByUsername(userName).then((rows) => {
      return mapUser(rows);
    })
  );
};

export const getLatestUser = async () => {
  return Promise.resolve(
    findLatestUser().then((rows) => {
      return mapUser(rows);
    })
  );
};

export const editUser = async (user: User) => {
  return Promise.resolve(
    updateUser(user).then((res) => {
      return res;
    })
  );
};

export const deleteUser = async (id: string) => {
  return Promise.resolve(
    removeUser(id).then((res) => {
      return res;
    })
  );
};

const mapUser = (rows: any) => {
  let userData = rows["_array"][0];
  if (userData == undefined) {
    return new UserBuilder()
      .setUserName("NOT_FOUND") //
      .build();
  } else {
    return new UserBuilder()
      .setId(userData.id) //
      .setUserName(userData.user_name) //
      .setFirstName(userData.first_name) //
      .setLastName(userData.last_name) //
      .setAge(userData.age) //
      .setEmail(userData.email) //
      .setGender(userData.gender) //
      .setPassword(userData.password) //
      .build();
  }
};
