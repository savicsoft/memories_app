import MemoryBuilder from "../db/model/builder/MemoryBuilder";
import Memory from "../db/model/Memory";
import {
  findMemory,
  insertMemory,
  findAllMemories,
  updateMemory,
  removeMemory,
} from "../db/repo/MemoryRepo";

export const addMemory = async (memory: Memory) => {
  return Promise.resolve(
    insertMemory(memory).then((res) => {
      return res;
    })
  );
};

export const getMemory = async (id: string) => {
  return Promise.resolve(
    findMemory(id).then((rows) => {
      return mapMemory(rows["_array"][0]);
    })
  );
};

export const getAllMemories = async (userId: string) => {
  return Promise.resolve(
    findAllMemories(userId).then((rows) => {
      return mapMemories(rows["_array"]);
    })
  );
};

export const editMemory = async (memory: Memory) => {
  return Promise.resolve(
    updateMemory(memory).then((res) => {
      return res;
    })
  );
};

export const deleteMemory = async (id: string) => {
  return Promise.resolve(
    removeMemory(id).then((res) => {
      return res;
    })
  );
};

const mapMemories = (memoriesData: any) => {
  let memories: Memory[];
  memoriesData.forEach((memory) => {
    memories.push(mapMemory(memory));
  });
  return memories;
};

const mapMemory = (memoryData: any) => {
  return new MemoryBuilder()
    .setId(memoryData.id) //
    .setUserId(memoryData.user_id) //
    .setTitle(memoryData.title) //
    .setDescription(memoryData.description) //
    .setImage(memoryData.image) //
    .setMoodIndicator(memoryData.mood_indicator) //
    .setDate(memoryData.date) //
    .build();
};
