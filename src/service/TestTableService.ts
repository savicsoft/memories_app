import { getTestTable } from "./../db/TestTable";

export const fetchTestTable = (id: string) => {
  return Promise.resolve(
    getTestTable(id).then((rows) => {
      return rows["_array"][0].value;
    })
  );
};
