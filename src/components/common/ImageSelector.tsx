import * as ImagePicker from "expo-image-picker";
import React, { useEffect, useState } from "react";
import { Image, Button, View, Platform } from "react-native";
import i18n from "i18n-js";
import { simpleAlert } from "../../service/AlertService";

const ImageSelector = ({ memory, setMemory }) => {
  const [isImagePicked, setIsImagePicked] = useState(false);

  useEffect(() => {
    async () => {
      if (Platform.OS !== "web") {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          simpleAlert(
            i18n.t("addMemoryAlertPermissionTitle"),
            i18n.t("addMemoryAlertLibraryPermissionDenied")
          );
        }
      }
    };
  }, []);

  const onPickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      //aspect: [4, 3],
      base64: true,
      quality: 1,
    });

    if (!result.cancelled) {
      setMemory({
        ...memory,
        imageUri: result["uri"],
        imageBytes: result["base64"],
      });
      setIsImagePicked(true);
    }
  };

  const showImage = () => {
    if (isImagePicked) {
      return (
        <Image
          source={{ uri: memory.imageUri }}
          style={{ width: 200, height: 200 }}
        />
      );
    }
  };

  return (
    <View>
      <Button title={i18n.t("imageSelectorButtonTxt")} onPress={onPickImage} />
      {showImage()}
    </View>
  );
};

export default ImageSelector;
