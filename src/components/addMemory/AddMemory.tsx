import React, { useState, useEffect } from "react";
import { ActivityIndicator, TextInput, Text, View, Button } from "react-native";
import i18n from "i18n-js";
import ImageSelector from "../common/ImageSelector";
import DateSelector from "../common/DateSelector";
import { simpleAlert } from "../../service/AlertService";
import MemoryBuilder from "../../db/model/builder/MemoryBuilder";
import { addMemory } from "../../service/MemoryService";
import Slider from "@react-native-community/slider";

const AddMemory = ({ user }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [memory, setMemory] = useState({
    title: "",
    description: "",
    imageBytes: "",
    imageUri: "",
    moodIndicator: "",
    date: new Date(),
  });

  const getView = () => {
    if (isLoading) {
      return <ActivityIndicator size="large" color="#0000ff" />;
    } else {
      return (
        <View>
          <Text>{i18n.t("addMemoryPageTitle")}</Text>
          <TextInput
            onChangeText={onChangeTitle}
            value={memory.title}
            placeholder={i18n.t("addMemoryTitlePlaceholder")}
          />
          <TextInput
            onChangeText={onChangeDescription}
            value={memory.description}
            placeholder={i18n.t("addMemoryDescriptionPlaceholder")}
            numberOfLines={4}
            multiline={true}
          />
          <ImageSelector memory={memory} setMemory={setMemory} />
          <DateSelector onChangeDate={onChangeDate} />
          <Slider
            style={{ width: 200, height: 40 }}
            minimumValue={0}
            maximumValue={10}
            step={1}
            onValueChange={onChangeMoodValue}
            minimumTrackTintColor="#FFFFFF"
            maximumTrackTintColor="#000000"
          />
          <Button
            title={i18n.t("addMemoryButtonTitle")}
            onPress={onAddMemory}
          />
        </View>
      );
    }
  };

  const onAddMemory = () => {
    if (!memory.title.trim()) {
      simpleAlert(
        i18n.t("addMemoryAlertTitle"),
        i18n.t("addMemoryAlertEmptyTitle")
      );
      return;
    }

    if (!memory.imageUri.trim()) {
      simpleAlert(
        i18n.t("addMemoryAlertTitle"),
        i18n.t("addMemoryAlertNoImage")
      );
      return;
    }

    setIsLoading(true);

    let memoryToAdd = new MemoryBuilder()
      .generateId() //
      .setUserId(user.id) //
      .setDate(memory.date.toUTCString()) //
      .setDescription(memory.description) //
      .setImage(memory.imageBytes) //
      .setTitle(memory.title) //
      .setMoodIndicator(memory.moodIndicator) //
      .build();

    addMemory(memoryToAdd).then(() => {
      console.log(memoryToAdd);
      setIsLoading(false);
    });
  };

  const onChangeTitle = (input) => {
    setMemory({ ...memory, title: input });
  };

  const onChangeDescription = (input) => {
    setMemory({ ...memory, description: input });
  };

  const onChangeDate = (input) => {
    setMemory({ ...memory, date: input });
  };

  const onChangeMoodValue = (input) => {
    setMemory({ ...memory, moodIndicator: input });
  };

  return getView();
};

export default AddMemory;
