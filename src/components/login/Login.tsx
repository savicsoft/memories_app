import React, { useState } from "react";
import { ActivityIndicator } from "react-native";
import { Button, Text, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { addUser } from "../../service/UserService";
import i18n from "i18n-js";
import UserBuilder from "../../db/model/builder/UserBuilder";
import { simpleAlert } from "../../service/AlertService";

const Login = ({ onLogin }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState({
    username: "",
    password: "",
    repeatPassword: "",
  });

  const login = () => {
    if (!user.username.trim()) {
      simpleAlert(i18n.t("loginAlertTitle"), i18n.t("loginEmptyUsername"));
      return;
    }

    if (!user.password.trim()) {
      simpleAlert(i18n.t("loginAlertTitle"), i18n.t("loginEmptyPassword"));
      return;
    }

    if (!user.repeatPassword.trim()) {
      simpleAlert(
        i18n.t("loginAlertTitle"),
        i18n.t("loginEmptyRepeatPassword")
      );
      return;
    }

    if (user.password !== user.repeatPassword) {
      simpleAlert(
        i18n.t("loginAlertTitle"),
        i18n.t("loginAlertPasswordMismatch")
      );
      return;
    }

    setIsLoading(true);

    let userToAdd = new UserBuilder()
      .generateId() //
      .setUserName(user.username) //
      .setPassword(user.password); //

    addUser(userToAdd).then(() => {
      onLogin();
    });
  };

  const getView = () => {
    if (isLoading) {
      return <ActivityIndicator size="large" color="#0000ff" />;
    } else {
      return (
        <View>
          <Text>{i18n.t("loginPageTitle")}</Text>
          <TextInput
            onChangeText={onChangeUsernameText}
            value={user.username}
            placeholder={i18n.t("loginPageUsernamePlaceholder")}
            textContentType="username"
          />
          <TextInput
            onChangeText={onChangePassword}
            value={user.password}
            placeholder={i18n.t("loginPasswordPlaceholder")}
            textContentType="password"
            secureTextEntry={true}
          />
          <TextInput
            onChangeText={onChangeRepeatPassword}
            value={user.repeatPassword}
            placeholder={i18n.t("loginRepeatPasswordPlaceholder")}
            textContentType="password"
            secureTextEntry={true}
          />
          <Button title={i18n.t("loginButtonTitle")} onPress={login} />
        </View>
      );
    }
  };

  const onChangeUsernameText = (input) => {
    setUser({ ...user, username: input });
  };

  const onChangePassword = (input) => {
    setUser({ ...user, password: input });
  };

  const onChangeRepeatPassword = (input) => {
    setUser({ ...user, repeatPassword: input });
  };

  return getView();
};

export default Login;
