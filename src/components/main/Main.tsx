import { ActivityIndicator, Text, View } from "react-native";
import React, { useEffect, useState } from "react";
import Home from "../home/Home";
import Login from "../login/Login";
import { getLatestUser } from "../../service/UserService";
import { dropTables, initializeTables } from "../../db/TableInitialization";

const Main = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [userExists, setUserExists] = useState(false);
  const [user, setUser] = useState({});

  useEffect(() => {
    initializeTables().then(() => {
      getUser();
    });
  }, []);

  const onLogin = () => {
    getUser();
  };

  const getUser = () => {
    getLatestUser().then((user) => {
      if (user.userName == "NOT_FOUND") {
        setIsLoading(false);
      } else {
        setUser(user);
        setUserExists(true);
        setIsLoading(false);
      }
    });
  };

  const getView = () => {
    if (isLoading) {
      return <ActivityIndicator size="large" color="#0000ff" />;
    } else if (!isLoading && userExists) {
      return <Home user={user}></Home>;
    } else if (!isLoading && !userExists) {
      return <Login onLogin={onLogin} />;
    }
  };

  return getView();
};

export default Main;
