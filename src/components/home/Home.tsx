import React from "react";
import { Text, View } from "react-native";
import AddMemory from "../addMemory/AddMemory";

const Home = ({ user }) => {
  return (
    <View>
      <AddMemory user={user} />
    </View>
  );
};

export default Home;
