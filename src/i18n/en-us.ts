const englishUnitedStates = {
  welcomeMsg: "Welcome to the app!",

  //Login page strings
  loginPageTitle: "Login Page",
  loginButtonTitle: "Login",
  loginPageUsernamePlaceholder: "Input your name...",
  loginPasswordPlaceholder: "Input your password...",
  loginRepeatPasswordPlaceholder: "Repeat your password...",
  loginAlertTitle: "Wrong input",
  loginAlertPasswordMismatch: "Password doesn't match!",
  loginEmptyUsername: "Empty username field!",
  loginEmptyPassword: "Empty password field!",
  loginEmptyRepeatPassword: "Empty repeat password field!",

  //Add Memory page strings
  addMemoryPageTitle: "Add a memory",
  addMemoryTitlePlaceholder: "Give your memory a title...",
  addMemoryDescriptionPlaceholder: "Give us a short description...",
  addMemoryAlertPermissionTitle: "Permission Error",
  addMemoryAlertLibraryPermissionDenied: "Camera roll permission is needed",
  addMemoryButtonTitle: "Add",
  addMemoryAlertTitle: "Missing Input",
  addMemoryAlertEmptyTitle: "Title is required.",
  addMemoryAlertNoImage: "No image selected",

  //Image Selector strings
  imageSelectorButtonTxt: "Pick Image",

  //Date Selector strings
  dateSelectorDate: "Pick date",
  dateSelectorTime: "Pick time",
};

export default englishUnitedStates;
