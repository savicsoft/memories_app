const serbianLatin = {
  welcomeMsg: "Dobrodošli na applikaciju!",

  //Login page strings
  loginPageTitle: "Login Stranica",
  loginButtonTitle: "Uloguj me",
  loginPageUsernamePlaceholder: "Upišite vaše nadimak...",
  loginPasswordPlaceholder: "Upišite lozinku...",
  loginRepeatPasswordPlaceholder: "Ponovite lozinku...",
  loginAlertTitle: "Pogrešan unos",
  loginAlertPasswordMismatch: "Lozinka se ne poklapa!",
  loginEmptyUsername: "Prazno polje za nadimak!",
  loginEmptyPassword: "Prazno polje za lozinku!",
  loginEmptyRepeatPassword: "Prazno polje za ponavljanje lozinke!",

  //Add memory strings
  addMemoryPageTitle: "Dodaj Sećanje",
  addMemoryTitlePlaceholder: "Daj svom sećanju titl...",
  addMemoryDescriptionPlaceholder: "Opiši ukratko tvoje sećanje...",
  addMemoryAlertPermissionTitle: "Greška sa permisijama",
  addMemoryAlertLibraryPermissionDenied: "Dodajte permisiju za camera roll",
  addMemoryButtonTitle: "Dodaj",
  addMemoryAlertTitle: "Fali Unos",
  addMemoryAlertEmptyTitle: "Titl nije unet",
  addMemoryAlertNoImage: "Nije odabrana slika",

  //Image Selector strings
  imageSelectorButtonTxt: "Izaberi sliku",

  //Date Selector strings
  dateSelectorDate: "Izaberi datum",
  dateSelectorTime: "Izaberi vreme",
};

export default serbianLatin;
