const swedishSweden = {
  welcomeMsg: "Välkommen i vår app!",

  //Login page strings
  loginPageTitle: "Logga in sida",
  loginButtonTitle: "Logga in",
  loginPageUsernamePlaceholder: "Skriv ditt namn...",
  loginPasswordPlaceholder: "Skriv ditt lösenord...",
  loginRepeatPasswordPlaceholder: "Skriv igen ditt lösenord...",
  loginAlertTitle: "Fel inmatning",
  loginAlertPasswordMismatch: "Lösenordet matchar inte!",
  loginEmptyUsername: "Tomt användarnamnfält!",
  loginEmptyPassword: "Tomt lösenordfält!",
  loginEmptyRepeatPassword: "Tomt upprepa lösenordsfält!",

  //Add memory strings
  addMemoryPageTitle: "Lägga till ett minne",
  addMemoryTitlePlaceholder: "Ge ditt minne en titel...",
  addMemoryDescriptionPlaceholder: "Beskriv ditt minne...",
  addMemoryAlertPermissionTitle: "Tillståndsfel",
  addMemoryAlertLibraryPermissionDenied: "Tillstånd camera roll behövs",
  addMemoryButtonTitle: "Spara",
  addMemoryAlertTitle: "Fel inmatning",
  addMemoryAlertEmptyTitle: "Tomt titel",
  addMemoryAlertNoImage: "Ingen bild",

  //Image Selector strings
  imageSelectorButtonTxt: "Välja bild",

  //Date Selector strings
  dateSelectorDate: "Välj datum",
  dateSelectorTime: "Välj tid",
};

export default swedishSweden;
