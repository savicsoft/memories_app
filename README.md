# README #

This file contains information about Memories APP and a guide on how to get started.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Requirments####

**Required**  
	
	* NodeJS LTS release.(https://nodejs.org/en/)  
	* GIT(https://git-scm.com/)  

**Optional**  
	
	* Visual Studio Code (https://code.visualstudio.com/)
	
#### Steps ####
	* Run <npm install --global expo-cli> on bash, powershell or cmd
	* Install Expo Go App on android or IOS testing device
	
#### VS CODE Recommended Extensions ####
	
	* GitLens  
	* EsLint  
	* Auto Close Tag  
	* Auto Rename Tag
	* Auto Import
	* Color Highlight
	* ES7 React/Redux/GraphQL/React-Native snippets
	* Highlight Matching Tag
	* Prettier - Code formatter
	* React Native Tools
	* vscode-styled-components


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact